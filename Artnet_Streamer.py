import sys;
import pyshark;
import struct;
import socket;
import time;
print("artnetStreamer");


UDP_IP = "127.0.0.1"
UDP_PORT = 6454

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM); # UDP
seq = 0;


p1 = struct.pack("8sHHBBHHBBBB",b"Art-Net",0x5000,14,seq,0,1,4,0,0,0,0);
p2 = struct.pack("8sHHBBHHBBBB",b"Art-Net",0x5000,14,seq,0,1,4,255,0,0,0);
while(True):
    print("send P1", p1);
    sock.sendto(p1, (UDP_IP, UDP_PORT));
    time.sleep(1);
    print("send P2", p2);
    sock.sendto(p2, (UDP_IP, UDP_PORT));
    time.sleep(1);
