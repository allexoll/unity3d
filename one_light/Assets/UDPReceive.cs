﻿using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPReceive : MonoBehaviour {

	public float duration = 1.0F;
	public Light lt;
	public float r = 0;
	public float g = 0;
	public float b = 0;
	// receiving Thread
	Thread receiveThread;

	// udpclient object
	UdpClient client;

	// public
	// public string IP = "127.0.0.1"; default local
	public int port; // define > init

	// infos
	public string lastReceivedUDPPacket="";
	public string allReceivedUDPPackets=""; // clean up this from time to time!

	// start from shell
	private static void Main()
	{
		UDPReceive receiveObj=new UDPReceive();
		receiveObj.init();

		string text="";
		do
		{
			text = Console.ReadLine();
		}
		while(!text.Equals("exit"));
	}
	// init
	private void init()
	{
		print("UDPSend.init()");

		// define port
		port = 6454;

		// status
		print("Sending to 127.0.0.1 : "+port);
		print("Test-Sending to this Port: nc -u 127.0.0.1  "+port+"");


		print ("start Receive");
		client = new UdpClient(port);

		print ("init client");


				receiveThread = new Thread(
					new ThreadStart(ReceiveData));
				receiveThread.IsBackground = true;
				receiveThread.Start();
		
	}
	void Start() {
		lt = GetComponent<Light>();
		init();

	}
	void Update() {
		//float phi = Time.time / duration * 2 * Mathf.PI;
		//float amplitude = Mathf.Cos(phi) * 0.5F + 0.5F;
		//lt.intensity = amplitude;
		//lt.intensity = 1;
		lt.color = new Color(r,g,b);

	}
	// OnGUI
	void OnGUI()
	{
		/*Rect rectObj=new Rect(40,10,200,400);
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.UpperLeft;
		GUI.Box(rectObj,"# UDPReceive\n127.0.0.1 "+port+" #\n"
			+ "shell> nc -u 127.0.0.1 : "+port+" \n"
			+ "\nLast Packet: \n"+ lastReceivedUDPPacket
			+ "\n\nAll Messages: \n"+allReceivedUDPPackets
			,style);*/
	}
	public string getLatestUDPPacket()
	{
		allReceivedUDPPackets="";
		return lastReceivedUDPPacket;
	}
	void OnDisable() 
	{ 
		if ( receiveThread!= null) 
			receiveThread.Abort(); 

		client.Close(); 
	} 
	void ReceiveData()
	{
		while(true)
		{
			try
			{
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
				byte[] data = client.Receive(ref anyIP);

				r = data[18]/(float)255.0;
				g = data[19]/(float)255.0;
				b = data[20]/(float)255.0;

				for(int i = 18; i < data.Length; i ++)
				{
					lastReceivedUDPPacket+= data[i].ToString()+ " ";
				}

				//print(">> " + data.ToString());


				//allReceivedUDPPackets=allReceivedUDPPackets+text;

			}
			catch (Exception err)
			{
				print(err.ToString());
			}
		}
	}
}

